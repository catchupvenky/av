{
	"version": "1.0",
	"session": {
		"new": false,
		"sessionId": "amzn1.echo-api.session.d95885d7-c260-4fa0-8812-2a7f2c90e2b9",
		"application": {
			"applicationId": "amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae"
		},
		"attributes": {
			"attr_deviceprotectionstep": "1",
			"attr_lastintent_threshold": "0",
			"attr_LastIntent_completed": "deviceprotection",
			"version": "20220330_15502",
			"attr_slotretry_threshold": "0",
			"attr_LastIntent_progress": "deviceprotection"
		},
		"user": {
			"userId": "amzn1.ask.account.AGGQZO4BVJHQ574CIUMHDH56GC6GMZYSIZV2QVSWS2JRQ3EPHRLD7AOZ5XINSHLAWGHJ7VI7DH7TYD63C5SX2VMYGS256Q7U2H5JG6HWX3AC3NILVAQ3PUI3PA6HFZKN6UL6QLDY5YD6VHCWZ4OB2LFFJB6OJNTJ4D33ZLCWLNR4UGZGUTRTRJ3QCOPAD7QUX5ZA2GVXQ7HBN7Q",
			"accessToken": "T6tPjRES003w4le0HSDsZSf8xLkY5cdPWTdH5sTkfG5OCIFCTzxyPp5je4L3kFgNDcJWyMUJtXgmaQZRj9SmFqMChzB0nmHletit"
		}
	},
	"context": {
		"Viewports": [
			{
				"type": "APL",
				"id": "main",
				"shape": "RECTANGLE",
				"dpi": 213,
				"presentationType": "STANDARD",
				"canRotate": false,
				"configuration": {
					"current": {
						"mode": "HUB",
						"video": {
							"codecs": [
								"H_264_42",
								"H_264_41"
							]
						},
						"size": {
							"type": "DISCRETE",
							"pixelWidth": 1280,
							"pixelHeight": 800
						}
					}
				}
			}
		],
		"Viewport": {
			"experiences": [
				{
					"arcMinuteWidth": 346,
					"arcMinuteHeight": 216,
					"canRotate": false,
					"canResize": false
				}
			],
			"mode": "HUB",
			"shape": "RECTANGLE",
			"pixelWidth": 1280,
			"pixelHeight": 800,
			"dpi": 213,
			"currentPixelWidth": 1280,
			"currentPixelHeight": 800,
			"touch": [
				"SINGLE"
			],
			"video": {
				"codecs": [
					"H_264_42",
					"H_264_41"
				]
			}
		},
		"Extensions": {
			"available": {
				"aplext:backstack:10": {}
			}
		},
		"System": {
			"application": {
				"applicationId": "amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae"
			},
			"user": {
				"userId": "amzn1.ask.account.AGGQZO4BVJHQ574CIUMHDH56GC6GMZYSIZV2QVSWS2JRQ3EPHRLD7AOZ5XINSHLAWGHJ7VI7DH7TYD63C5SX2VMYGS256Q7U2H5JG6HWX3AC3NILVAQ3PUI3PA6HFZKN6UL6QLDY5YD6VHCWZ4OB2LFFJB6OJNTJ4D33ZLCWLNR4UGZGUTRTRJ3QCOPAD7QUX5ZA2GVXQ7HBN7Q",
				"accessToken": "T6tPjRES003w4le0HSDsZSf8xLkY5cdPWTdH5sTkfG5OCIFCTzxyPp5je4L3kFgNDcJWyMUJtXgmaQZRj9SmFqMChzB0nmHletit"
			},
			"device": {
				"deviceId": "amzn1.ask.device.AF623AMUA6PCVYWF35IW3G7IMI422SS2QWT6EQZYYEBAA45R4U3TVSUEQF7UATSL6Z6IVY4B4XXRH5NPDTNXLE5SPIAG7WGFDB7Z4Q67S5IBGMI43U64XI4P2WOFZTWVOEQYFZRQZCYZ25V5LLXH2MD2STJ4ENRNOH6RHEUPZ3BQSLL2AJ7LK",
				"supportedInterfaces": {}
			},
			"apiEndpoint": "https://api.amazonalexa.com",
			"apiAccessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IjEifQ.eyJhdWQiOiJodHRwczovL2FwaS5hbWF6b25hbGV4YS5jb20iLCJpc3MiOiJBbGV4YVNraWxsS2l0Iiwic3ViIjoiYW16bjEuYXNrLnNraWxsLjhiMzllOGU5LWY5MzgtNDUyMy05YjIxLTljMDRiNzdiOGZhZSIsImV4cCI6MTY0ODQ1NzYyMywiaWF0IjoxNjQ4NDU3MzIzLCJuYmYiOjE2NDg0NTczMjMsInByaXZhdGVDbGFpbXMiOnsiY29udGV4dCI6IkFBQUFBQUFBQUFEeHdzOXpDQVV6OTczS2s0MS9HWVd3S3dFQUFBQUFBQUFreVRsVEdsWGREOC8rckNUUng1MzdNWkRsYjIzeWx5NEpkdFRpa1FZV3JGVjBNdXBBVTdscFlmUDYzczNoRFhaNHp4WFZMT3BuSWZ5YmwrSlRxMmR2UG1EUEE0TnBHUFZ1dDVBNmhxTjlUMkNvZU1JeThzNVFYN1FYejAzSTNmUmtMU2RnY0hFNDFWYkdDOC9pUnFtVktiR1puN1Z2RFpjT1lxVVN0YlNBeEQvRjNKc3VTQzhCUFVhcDI5ZVZwSXo4dmFpY05MMEc2TTQxWDU4c1A1RjExTG8yRHhYOGN4RFZNajl3cGpvMGZWV3puSWZNTkxsUzdrMGQrZStKdGhaMmplR1MyN0d0MGcyRFc5S1djZWZMVGJEN20xNjdQbzFURFRFcFRtYmdWU213dkE2WW0wU1VsRXJpNUxYRTVWNEhwQzhkUGthdE5tblNISFhyeVl4bTdrdUkvejcyYThZSjgvQU96SWVBOVBqL1NwTGFvSkM0RkhWOHVsWTNlU09UUUcyUzZseHhuelRUMVE9PSIsImNvbnNlbnRUb2tlbiI6bnVsbCwiZGV2aWNlSWQiOiJhbXpuMS5hc2suZGV2aWNlLkFGNjIzQU1VQTZQQ1ZZV0YzNUlXM0c3SU1JNDIyU1MyUVdUNkVRWllZRUJBQTQ1UjRVM1RWU1VFUUY3VUFUU0w2WjZJVlk0QjRYWFJINU5QRFROWExFNVNQSUFHN1dHRkRCN1o0UTY3UzVJQkdNSTQzVTY0WEk0UDJXT0ZaVFdWT0VRWUZaUlFaQ1laMjVWNUxMWEgyTUQyU1RKNEVOUk5PSDZSSEVVUFozQlFTTEwyQUo3TEsiLCJ1c2VySWQiOiJhbXpuMS5hc2suYWNjb3VudC5BR0dRWk80QlZKSFE1NzRDSVVNSERINTZHQzZHTVpZU0laVjJRVlNXUzJKUlEzRVBIUkxEN0FPWjVYSU5TSExBV0dISjdWSTdESDdUWUQ2M0M1U1gyVk1ZR1MyNTZRN1UySDVKRzZIV1gzQUMzTklMVkFRM1BVSTNQQTZIRlpLTjZVTDZRTERZNVlENlZIQ1daNE9CMkxGRkpCNk9KTlRKNEQzM1pMQ1dMTlI0VUdaR1VUUlRSSjNRQ09QQUQ3UVVYNVpBMkdWWFE3SEJON1EifX0.kghn3S4Eavo4Z4VyM-P98HwKszRwBTSx7__MgGHEXgx138zimhINSU7QtnIf0JAB_SxdfoPVL2n5-0YX7CCjCiZWdBcxVvi20eWauGoo5JTYETiVmYH6PBRkafkbdWDJUMCiOm3Y8FXyqqpYxZ7ZS6h7R0-NkzhxMBKxVrcM8VzJyF2dGClvXCEJQAS54mlgqd-WcxyW05iqhajJnViD_wM4Ilf2kcbiW0MSj8RRis1m0ejOhbHOBj3SR-_v4_TcDOP7hIo-9J33IxHMUEFbUBWEPq17FIKUXOYMd7rewcOXF5_VUfP_rJd21YHmV-lhCFR0XZsimItA9eYz0dAxUw"
		}
	},
	"request": {
		"type": "IntentRequest",
		"requestId": "amzn1.echo-api.request.e0c7fe07-35bd-432e-afa5-497b857058f1",
		"locale": "en-US",
		"timestamp": "2022-03-28T08:48:43Z",
		"intent": {
			"name": "deviceprotection",
			"confirmationStatus": "NONE",
			"slots": {
				"protection": {
					"name": "protection",
					"value": "insurance",
					"resolutions": {
						"resolutionsPerAuthority": [
							{
								"authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae.Protection",
								"status": {
									"code": "ER_SUCCESS_MATCH"
								},
								"values": [
									{
										"value": {
											"name": "deviceprotection",
											"id": "da6792e58aea99b904c6cf8e45db93ec"
										}
									}
								]
							}
						]
					},
					"confirmationStatus": "NONE",
					"source": "USER",
					"slotValue": {
						"type": "Simple",
						"value": "insurance",
						"resolutions": {
							"resolutionsPerAuthority": [
								{
									"authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae.Protection",
									"status": {
										"code": "ER_SUCCESS_MATCH"
									},
									"values": [
										{
											"value": {
												"name": "deviceprotection",
												"id": "da6792e58aea99b904c6cf8e45db93ec"
											}
										}
									]
								}
							]
						}
					}
				},
				"userconfirmation": {
					"name": "userconfirmation",
					"value": "yes",
					"resolutions": {
						"resolutionsPerAuthority": [
							{
								"authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae.YesOrNo",
								"status": {
									"code": "ER_SUCCESS_MATCH"
								},
								"values": [
									{
										"value": {
											"name": "Yes",
											"id": "Yes"
										}
									}
								]
							}
						]
					},
					"confirmationStatus": "NONE",
					"source": "USER",
					"slotValue": {
						"type": "Simple",
						"value": "yes",
						"resolutions": {
							"resolutionsPerAuthority": [
								{
									"authority": "amzn1.er-authority.echo-sdk.amzn1.ask.skill.8b39e8e9-f938-4523-9b21-9c04b77b8fae.YesOrNo",
									"status": {
										"code": "ER_SUCCESS_MATCH"
									},
									"values": [
										{
											"value": {
												"name": "Yes",
												"id": "Yes"
											}
										}
									]
								}
							]
						}
					}
				},
				"userchoice": {
					"name": "userchoice",
					"confirmationStatus": "NONE"
				}
			}
		},
		"dialogState": "IN_PROGRESS"
	}
}
